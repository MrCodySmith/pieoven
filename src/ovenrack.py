#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys

from utils import config
from utils.disk import MountedImage
from utils.modules.base import Module


def prechecks(args):
    # Check that the User is Root
    if os.geteuid() != 0 and not args.dry_run:
        sys.exit('You must be root to run this program!')

    # Check that we're on ubuntu ( for now )
    opsys = os.uname()
    opsys = opsys.version

    if 'Ubuntu' not in opsys:
        print(f'Current OS Version: {opsys}')
        sys.exit('You must run this script on Ubuntu for now.')


def main():
    argparser = argparse.ArgumentParser()
    argparser.add_argument('--config', help='The recipe file to use')
    argparser.add_argument('--dry-run', action='store_true')
    args = argparser.parse_args()

    prechecks(args)

    if args.config:
        cfg = config.ParseConfig(args.config)
        modules = config.LoadModules(cfg)
        recipe = config.MakeRecipe(cfg, modules)

        from pprint import pprint
        pprint(cfg.data)
        pprint(f"{modules=}")
        print(f"{recipe=}")

        print('--------------------------------------------------------------------')
        data = recipe.generate_config()
        print('--------------------------------------------------------------------')
        pprint(f"{data=}")

        recipe = Module(cfg)

        if errors_list := recipe.handle_modules():
            for error in errors_list:
                print(error)
            print()
            # sys.exit(-1)  commented out for now because we're missing modules and we know it
        for instruction in recipe.get_instructions():
            print(instruction)
        print('')

        if args.dry_run:
            sys.exit()

        with MountedImage(cfg) as image:
            input("DEBUG: Press enter to execute instructions. ")
            recipe.execute_instructions(image)
            print('Instructions executed!')
            input("DEBUG: Press enter to unmount and quit. ")
            print("DEBUG: Unmounted.")

    elif not args.config:
        sys.exit('Please specifiy a configuration file with --config')


if __name__ == '__main__':
    main()
