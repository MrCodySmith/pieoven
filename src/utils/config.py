#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys

import yaml

from .utilities import get_list, merge_yaml, load_modules
from utils.modules.base import ModuleRoot


class Config():
    def __init__(self, filename):
        self.env = {}
        self.original_path = os.path.dirname(filename)
        self.full_path = self.get_filename(filename)
        self.filename = filename
        self.errors = []
        try:
            with open('.env', 'r') as f:
                for line in f:
                    key, value = line.strip().split('=')
                    self.env[key] = value
        except FileNotFoundError:
            pass
        with open(self.full_path, 'r') as f:
            self.data = self.postprocess(yaml.load(f, yaml.SafeLoader))


    def __repr__(self):
        return self.filename

    def dumps(self):
        return yaml.safe_dump(self.data, indent=4)

    def get_filename(self, path):
        """ Given a YML file, determine the full path the file. First check the local directory,
        then the local directory modified by the original path to the config, then the defaults directory.
        """
        default = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'configs', path)
        relative = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', self.original_path, path)
        if os.path.isfile(path):
            return path
        elif os.path.isfile(relative):
            return relative
        elif os.path.isfile(default):
            return default
        else:
            raise self.OvenRackConfigException(f"The recipe {path} cannot be found.")

    def postprocess(self, data):
        ret = {}
        if 'setenv' in data:
            for pair in data['setenv']:
                for key in pair:
                    self.env[key] = pair[key]
            for key in self.env:
                if not os.environ.get(key, ''):
                    os.environ[key] = str(self.env[key])
            del data['setenv']
        if 'image' in data:
            image_default = f"{data['image']}.yaml"
            if 'include' in data:
                data['include'] = get_list(data['include']) + [image_default, ]
            else:
                data['include'] = [image_default, ]
            del data['image']
        if 'include' in data:
            include_list = get_list(data['include'])
            del data['include']
            for filename in include_list:
                full_path = self.get_filename(filename)
                with open(full_path, 'r') as f:
                    new = self.postprocess(yaml.load(f, yaml.SafeLoader))
                    ret = merge_yaml(ret, new)
        ret = merge_yaml(ret, data)
        return ret

    class OvenRackConfigException(Exception):
        pass


def ParseConfig(path):
    if os.path.exists(path):
        config = Config(path)
        return config

    elif not os.path.exists(path):
        print(f'The config file: {path} Doesn\'t exist!')
        sys.exit('Config file not found')


def LoadModules(config):
    return load_modules(config.data.pop('load', []))


def MakeRecipe(config, modules):
    return ModuleRoot(config.data, modules)
