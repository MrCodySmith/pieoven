#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import copy
import os
import importlib
import traceback
from functools import wraps


def get_list(obj, default=[]):
    """ Many things in our recipes can be either a value or a list of values. get_list allows us to
        always treat these values as a list. We can also assume a default if there is no value there."""
    if not obj:
        return default
    if isinstance(obj, list):
        return obj
    else:
        return [obj, ]


def merge_yaml(original, new):
    """ Do a non-destructive deep merge of two YAML configs. The second config will be preferred,
        and any lists will be concatenated, unless the list is suffixed with a bang (!).
        In that case, the list of the first is replaced with the list of the second."""
    merged = copy.deepcopy(original)
    for orig_obj in new:
        obj = orig_obj.rstrip('!')
        if obj in merged:
            if isinstance(original[obj], dict):
                merged[obj] = merge_yaml(original[obj], new[orig_obj])
            elif isinstance(original[obj], list):
                if orig_obj[-1] == '!':
                    merged[obj] = get_list(new[orig_obj])
                else:
                    merged[obj] = get_list(original[obj]) + get_list(new[orig_obj])
            else:
                merged[obj] = new[orig_obj]
        else:
            merged[obj] = new[orig_obj]
    return merged


def progress_bar(block_num, block_size, total_size):
    """ Give a progress bar for the completion rate of an image download. """
    console_width = os.get_terminal_size().columns
    width = console_width - 10
    percent = (block_num * block_size) / total_size
    progress = '#' * int(percent * width) + ' ' * (width - int(percent * width))
    percent_done = str(int(percent * 100)).rjust(3) + "%"
    print(f"\r[ {progress} {percent_done} ]", end='')


registered_services = {}


def load_modules(extra_modules):
    modules = {}
    base_modules = ['image-url', 'install', 'package-manager', 'post', 'services']

    default = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'modules')
    relative = os.path.join(os.path.realpath(os.getcwd()), 'modules')
    files = []
    for path in [default, relative]:
        if os.path.exists(path):
            files += [(path, file) for file in os.listdir(path)]
    for path, file in files:
        if file.endswith('.py'):
            try:
                spec = importlib.util.spec_from_file_location(file, os.path.join(path, file))
                module = importlib.util.module_from_spec(spec)
                spec.loader.exec_module(module)
            except:
                traceback.print_exc()

    return registered_services


def service(service_name):
    def decorate(cls):
        @wraps(cls)
        def wrapper():
            return cls
        registered_services[service_name] = cls
        return wrapper
    return decorate
