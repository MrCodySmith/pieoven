#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class BaseServiceField(object):
    field_name = ''
    required = False
    contents = None
    loaded_modules = None

    def __init__(self, field_name, required=False):
        self.field_name = field_name
        self.required = required
        self.contents = []

    def __repr__(self):
        if self.field_name:
            return f"<{self.__class__} '{self.field_name}'>"
        else:
            return f"<{self.__class__} (Service Root)>"

    def get_module(self, path_name, config):
        module = self.loaded_modules.get(path_name, None)
        if module:
            return module(config, self.loaded_modules)
        return module

    def get_data(self, tree, path):
        self.contents = tree
        return self.contents


class RecursiveListField(BaseServiceField):
    def get_data(self, tree, path):
        try:
            list_tree = list(tree)
        except:
            raise Exception(f"Expected list for '{'.'.join(path)}.{self.field_name}', got '{type(tree)}'.")
            return
        self.contents = []
        for subservice in list_tree:
            current_path = path + [subservice]
            path_name = '.'.join(current_path)
            if module := self.get_module(path_name, tree[subservice]):
                self.contents.append(module.generate_config(current_path))
            else:
                print(f"Module {path_name} not found or loaded.")
        if len(self.contents) == 1:
            self.contents = self.contents[0]
        return self.contents


class ListField(BaseServiceField):
    def get_data(self, tree, path):
        try:
            self.contents = list(tree)
            return self.contents
        except:
            raise Exception(f"Invalid data for field {path}.{self.field_name}, not a list.")


class StringField(BaseServiceField):
    def get_data(self, tree, path):
        try:
            self.contents = str(tree)
            return self.contents
        except:
            raise Exception(f"Invalid data for field {path}.{self.field_name}, not a string.")
