#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pathlib
import shutil
import struct
import tempfile
import time
import urllib.request
import zipfile

from .utilities import progress_bar


class MountedImage(object):
    """
    Mount an image in it's entirety, and then return a handler where it can be modified by an
    Instruction. Provides path() to access things within the mount.
    """
    def __init__(self, config):
        self.config = config
        self.mounted_paths = []

    def __enter__(self):
        self.image = self._get_image()  # get image from internet or cache, then make a working copy
        self.boot_partition, self.partitions = self._do_fdisk()  # get the boot partition and the rest.
        self.root_partition = self._examine_bootloader()  # mount the bootloader and find /.
        self.fstab = self._get_fstab()  # mount / and read /etc/fstab.
        self._finish_mounting()  # mount all the partitions now that we know their mount points.
        print(self.guest_root)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        for folder in self.mounted_paths[::-1]:
            successfully_unmounted = False
            while not successfully_unmounted:
                try:
                    os.system(f"/bin/umount {folder}")
                    os.rmdir(folder)
                except Exception:
                    print(f"Resouce {folder} is busy. Retrying...")
                    time.sleep(5)
                finally:
                    successfully_unmounted = True

    def path(self, full_path):
        if full_path.startswith('~'):
            OSError("Destination path {full_path} cannot include a ~ character- target user is ambiguous.")
        return self._get_guest_path(full_path.lstrip(os.path.sep))

    def _get_host_path(self, *path):
        """
        Get a file on the host computer relative to ovenrack itself.
        """
        return os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', *path)

    def _get_guest_path(self, *path):
        """
        Get a file in the host image.
        """
        return os.path.join(self.guest_root, *path)

    def _get_image(self):
        """
        Get the image mentioned in the config either from cache or the internet, then make a working
        copy for us to work with.
        """
        image_url = self.config.data['image-url']
        image_name = pathlib.Path(image_url).stem
        local_images_folder = self._get_host_path('images')

        if not os.path.exists(local_images_folder):
            os.mkdir(local_images_folder)

        image_path = self._get_host_path('images', image_name + ".img")
        if not os.path.exists(image_path):
            print(f"Downloading {image_name}...")
            filename, headers = urllib.request.urlretrieve(image_url, reporthook=progress_bar)
            print(f"\nUnzipping {image_name}...")
            with zipfile.ZipFile(filename) as zip:
                zip.extractall(local_images_folder)
            os.remove(filename)
        else:
            print(f"Using images/{image_name}.img")

        os.getcwd()
        new_path = os.path.join(os.getcwd(), f"{self.config.filename.rsplit('.', 1)[0]}.img")
        print('Creating image...')
        try:
            os.remove(new_path)
        except FileNotFoundError:
            pass
        shutil.copy(image_path, new_path)
        return new_path

    def _do_fdisk(self):
        """
        Examine our image file and look at the MBR so we can get a list of all the partitions in it.
        We're also particularly interested in the bootloader, so we save that for the next step.
        """
        partitions = {}
        # MBR is 512 bytes long and starts with the magic number U\xaa.
        with open(self.image, 'rb') as f:
            mbr = f.read(512)
        if not mbr[-2:] == b'U\xaa':  # 0x55 0xAA
            raise OSError("Invalid or corrupted primary image.")
        UUID = mbr[440:444][::-1].hex()
        part_entries = mbr[446:-2]  # 64 bytes, 4x 16byte partition entries.
        part_entries = [part_entries[start:start + 16] for start in range(0, 64, 16)]
        part_i = 1
        for part_entry in part_entries:
            uuid = f"{UUID}-{part_i:0>2}"
            names = ['status', 'start_CHS', 'type', 'end_CHS', 'start_sector', 'num_sectors']
            partition_info = struct.unpack('<b3sb3sii', part_entry)
            partition_info = dict(zip(names, partition_info))
            if partition_info['num_sectors'] > 0:
                partitions[uuid] = partition_info
            if part_i == 1:
                boot_partition = partition_info
                if partition_info['type'] == 0xEE:
                    raise OSError("GPT partitioning found. Only MBR is currently supported.")
            part_i += 1

        return boot_partition, partitions

    def _examine_bootloader(self):
        """
        Temp mount the first partition (boot_partition) that we set aside earlier. This (should) be
        /boot, and contain some reference to kernel arguments via the bootloader.
        With an rpi, that's cmdline.txt.
        We want the root partition, because that's where /etc/fstab is.
        """
        root = ''
        offset = 512 * self.boot_partition['start_sector']
        sizelimit = 512 * self.boot_partition['num_sectors']
        try:
            self.guest_root = tempfile.mkdtemp(prefix='ovenrack_')
            options = f"loop,rw,offset={offset},sizelimit={sizelimit}"
            cmd = f"/bin/mount -t auto -o {options} {self.image} {self.guest_root}"
            os.system(cmd)
            # Now we have to figure out what kind of bootloader is being used.
            if os.path.exists(self._get_guest_path("cmdline.txt")):
                with open(self._get_guest_path("cmdline.txt")) as f:
                    cmdline = f.read().split(' ')
                    cmd = {arg.split('=')[0]: arg.split('=')[-1] for arg in cmdline}
                    root = cmd['root']
            else:
                raise OSError("Unknown bootloader. Cannot resolve rootfs.")
        finally:
            os.system(f"/bin/umount {self.guest_root}")
            os.rmdir(self.guest_root)
        return root

    def _get_fstab(self):
        """
        Now that we know which partition is the root partition, mount it, and read /etc/fstab so we know
        what other filesystems we have.
        """
        self.guest_root = tempfile.mkdtemp(prefix='ovenrack_')
        fstab = {}

        if self.root_partition in self.partitions:
            partition = self.partitions[self.root_partition]
            offset = 512 * partition['start_sector']
            sizelimit = 512 * partition['num_sectors']
            options = f"loop,rw,offset={offset},sizelimit={sizelimit}"
            cmd = f"/bin/mount -t auto -o {options} {self.image} {self.guest_root}"
            os.system(cmd)
            self.mounted_paths.append(self.guest_root)
            with open(self._get_guest_path("etc/fstab"), ) as f:
                for line in f.readlines():
                    if line.strip()[0] == "#" or line.strip() == "":
                        continue
                    # device, mount_point, fs, options, dump, pass
                    names = ['device', 'mount_point', 'fs', 'options', 'dump', 'fspass']
                    fstab_args = line.split()
                    fstab_args = dict(zip(names, fstab_args))
                    fstab[fstab_args['device']] = fstab_args
        else:
            raise OSError(f"Kernel rootfs {self.root_partition} not found in partition table.")
        return fstab

    def _finish_mounting(self):
        """
        We have our full list of partitions to mountpoints. Mount them all.
        """
        for device in self.fstab:
            if device.startswith('PARTUUID='):
                PARTUUID = device.replace('PARTUUID=', '', 1)
                if PARTUUID == self.root_partition:
                    continue  # This is already mounted, skip.
                if PARTUUID in self.partitions:
                    partition = self.partitions[PARTUUID]
                    offset = 512 * partition['start_sector']
                    sizelimit = 512 * partition['num_sectors']
                    options = f"loop,rw,offset={offset},sizelimit={sizelimit}"
                    mount_point = self._get_guest_path(self.fstab[device]['mount_point'].lstrip(os.path.sep))
                    cmd = f"/bin/mount -t auto -o {options} {self.image} {mount_point}"
                    os.system(cmd)
                    self.mounted_paths.append(mount_point)
            else:  # Skip it, it's a tmpfs, proc, or something else unsupported for now.
                pass
