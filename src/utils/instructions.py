#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re
from shutil import copy


class Instruction(object):
    """ Base Instruction class all instructions inherit from. """
    def _mkdirs(self, module, path):
        path = path.rstrip(os.path.sep)
        path_parts = path.split(os.path.sep)
        path = path_parts.pop(0)
        if not path:
            path = os.path.sep
        while os.path.exists(module.path(path)) and path_parts:
            path = os.path.join(path, path_parts.pop(0))
        while len(path_parts) > 0:
            os.mkdir(module.path(path), mode=self.permissions)
            os.chown(module.path(path), self.uid, self.gid)
            path = os.path.join(path, path_parts.pop(0))
        try:
            os.mkdir(module.path(path), mode=self.permissions)
            os.chown(module.path(path), self.uid, self.gid)
        except FileExistsError:
            pass


class ModifyFile(Instruction):
    """ Takes a file in the image and applies a `sed -i -e {change}` for each change. """
    def __init__(self, fpath, pattern, replace):
        self.fpath = os.path.expanduser(fpath)
        self.pattern = pattern
        self.replace = replace

    def execute(self, module):
        with open(module.path(self.fpath), "r+") as f:
            contents = f.read()
            contents = re.sub(self.pattern, self.replace, contents, flags=re.M | re.S)
            f.seek(0)
            f.truncate()
            f.write(contents)

    def __repr__(self):
        return f"[ModifyFile '{self.fpath}' 's/{repr(self.pattern)}/{repr(self.replace)}/gm']"


class CreateFile(Instruction):
    """ Given a path, creates a file with the contents given. """
    def __init__(self, recipe_fpath, contents, uid=0, gid=0, permissions=0o600):
        self.fpath = os.path.expanduser(recipe_fpath)
        self.contents = contents
        self.uid = uid
        self.gid = gid
        self.permissions = permissions

    def execute(self, module):
        self._mkdirs(module, os.path.split(self.fpath)[0])
        with open(module.path(self.fpath), 'x') as f:
            f.write(self.contents)
        os.chmod(module.path(self.fpath), self.permissions)
        os.chown(module.path(self.fpath), self.uid, self.gid)

    def __repr__(self):
        return f"[CreateFile '{self.fpath}']"


class CreateDir(Instruction):
    """ Given a path, creates a dir with the permissions given. """
    def __init__(self, recipe_fpath, uid=0, gid=0, permissions=0o600):
        self.fpath = os.path.expanduser(recipe_fpath)
        self.permissions = permissions
        self.uid = uid
        self.gid = gid
        self.permissions = permissions

    def execute(self, module):
        self._mkdirs(module, self.fpath)

    def __repr__(self):
        return f"[CreateDir '{self.fpath}']"


class CopyFile(Instruction):
    """ Given a local file, copy it into the image. """
    def __init__(self, host_fpath, recipe_fpath, uid=0, gid=0, permissions=0o600):
        self.host_fpath = os.path.expanduser(host_fpath)
        self.recipe_fpath = os.path.expanduser(recipe_fpath)
        self.uid = uid
        self.gid = gid
        self.permissions = permissions

    def execute(self, module):
        self._mkdirs(module, os.path.split(self.recipe_fpath)[0])
        copy(self.host_fpath, module.path(self.recipe_fpath))
        os.chown(module.path(self.recipe_fpath), self.uid, self.gid)
        os.chmod(module.path(self.recipe_fpath), self.permissions)

    def __repr__(self):
        return f"[CopyFile '{self.host_fpath}' -> '{self.recipe_fpath}']"


class SymLinkFile(Instruction):
    """ Given a file, symlink it to another file in the image. """
    def __init__(self, symlink_fpath, target_fpath):
        self.symlink_fpath = os.path.expanduser(symlink_fpath)
        self.target_fpath = os.path.expanduser(target_fpath)

    def execute(self, module):
        try:
            os.remove(module.path(self.target_fpath))
        except FileNotFoundError:
            pass
        os.symlink(module.path(self.symlink_fpath), module.path(self.target_fpath))

    def __repr__(self):
        return f"[SymLinkFile '{self.symlink_fpath}' -> '{self.target_fpath}']"


class ExecuteShell(Instruction):
    """ Execute some shell action in the image. Can only be used on devices that support os.system,
    and should only be used as a last resort. Creating a custom module is typically a better idea."""
    def __init__(self, shstr):
        self.shstr = shstr

    def execute(self, module):
        pid = os.fork()
        if pid == 0:
            # Child process
            os.chroot(module.rootfs)
            os.system(self.shstr)
            os._exit(0)  # We do this to avoid accidentally triggering pioven.py's finally on L63 twice.
        else:
            os.wait()

    def __repr__(self):
        return f"[ExecuteShell '{self.shstr}']"
