# PieOven

Using a configuration file, generate an up-to-date OS Image for quick deployment. 

`ovenrack.py [-h] [--config CONFIG] [--dry-run]`

Configs are YAML files. Examples can be found in examples/
